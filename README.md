# RNADockerMachineOnMacManager

BASICS

Start Docker VMs with docker-machine at macOS boot. This program reads one
or more JSON files that define docker machines, including which VM provider to
use (currently only VirtualBox can be detected), as what user the machine must
be started, the working directory to go to before starting or stopping a
machine, and the name of the docker machine. Example:
```
  [
    {
    "entryname": "john-default", # required, used for --only flag
    "displayname": "John's default docker machine",
                                 # Default: entryname
    "vmdriver": "virtualbox",    # VM driver to use
                                 # Default: virtualbox
    "user": "sysbh",             # User to run as
                                 # Default: the user that runs the script
                                 # (in which case no sudo/root is needed)
    "workingdir": "/Users/john", # Dir to cd to before running docker-machine
                                 # Default: environment value CWD or else
                                 # /private/tmp
    "machinename": "default",    # Docker machine name
                                 # Default: entryname
    "createoptions": ["--virtualbox-hostonly-cidr","192.168.98.1/24"],
                                 # Example. Sets network for DHCP on machine
                                 # and thus reliable IP address (workaround)
                                 # Default: nothing
    "enabled": true              # Set to false to ignore entry
                                 # Default: true
    },
    {
      "entryname": "gerben-lunaservices",
      "displayname": "Gerben's lunaservices docker machine",
      "vmdriver": "vmware",
      "user": "gerben",
      "workingdir": "/Users/gerben",
      "machinename": "lunaservices",
      "enabled": false
    },
    {
      "entryname" : "default"
    }
  ]
```
(Note: vmware support is not implemented) (Also Note: comments are not allowed
in JSON files, they're only here for explanation.)

Preferred install of this script:
 ```
   sudo cp macos-manage-docker-machines /usr/local/bin
    sudo chmod +x /usr/local/bin/macos-manage-docker-machines
```

The default json file used is:
`    /usr/local/etc/managed-docker-machines/default.json`
(can be overridden by the MANAGEDOCKERMACHINESDEFAULT environment variable)

The script can install a barebones default configuration for you which
contains a definition that can be shared by all users:
```
[
    {
      "entryname" : "generic",
      "machinename" : "default"
    }
]
```
This default machine definitions json lets this script act on the 'default'
docker-machine for any user, start the machine in the current directory
and uses virtualbox. It doesn't require root/sudo. It also doesn't manage
anything when the script is run as root, because root machines require
explicit
`    "user": "root"`
in the entry

Actually, it could have been simpler still (but potentially confusing):
```
[
    {
      "entryname" : "default"
    }
]
```
as the entryname is used also as machinename if no machinename is given.

So, after running
`    sudo macos-manage-docker-machines installdefault`
You can run
`    macos-manage-docker-machines create`
and get a 'default' docker machine.

START DOCKER MACHINES AT BOOT

To install a basic auto-launch configuration:
`    sudo macos-manage-docker-machines startatboot`
Note that this will not start any docker-machine at boot if the default json
is used, as the launchd runs as root and the default configuration doesn't have
an entry that works for root. So to make your personal machine 'mymachine'
start at boot you could add this entry to
`    /usr/local/etc/managed-docker-machines/myown.json`
(change 'me' to your own user name, twice)
```
[
  {
    "entryname":     "mymachine",
    "displayname":   "My beautiful docker machine",
    "vmdriver":      "virtualbox",
    "user":          "me",
    "workingdir":    "/Users/me",
    "machinename":   "mymachine",
    "createoptions": ["--virtualbox-hostonly-cidr","192.168.98.1/24"],
    "enabled":       true
  }
]
```

Note: the create-options example is a workaround to make sure your machine gets the
same IP address every time. For other machines, you change the third number of
the IP address, e.g.
`    "createoptions": ["--virtualbox-hostonly-cidr","192.168.97.1/24"],`
That way you can make sure that each machine has a reliable, repeatable IP
address.

And then you could run
`    macos-manage-docker-machines create /usr/local/etc/managed-docker-machines/myown.json`
to create your docker machine 'mymachine'

then the way to get these to run at macOS start is
`    sudo macos-manage-docker-machines startatboot /usr/local/etc/managed-docker-machines/myown.json`
or, for instance if you have multiple files with definitions:
`    sudo macos-manage-docker-machines --maxwait 120 startatboot /usr/local/etc/managed-docker-machines/*`

OVERRIDES

If the combination of machinename and user have already been managed
in a run of this script, a next entry will be ignored. Thus, you can create
definitions that override the later definitions. This is particularly useful
for getting reliable IP addresses on reboot/restart (see example above).
